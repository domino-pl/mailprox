import config
import asyncio
import logging
from datetime import datetime
import base64
import binascii
import re
from abc import ABC, abstractmethod
import signal
import time
import ssl
import sqlite3
import hash
from os import path


def make_path_abs(file_path: str) -> str:
	if not path.isabs(config.ssl_cert_file):
		file_path = path.join(path.dirname(__file__), file_path)
	return file_path


""" Setup logging """
logging.basicConfig(level="DEBUG")
log = logging.getLogger()
log.setLevel(config.log_level)

# add additional log file
log.removeHandler(log.handlers[0])
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
console_handler = logging.StreamHandler()
console_handler.setFormatter(formatter)
log.addHandler(console_handler)
if config.log_file is not None:
	file_handler = logging.FileHandler(make_path_abs(config.log_file))
	file_handler.setFormatter(formatter)
	log.addHandler(file_handler)

if log.level == logging.DEBUG:
	log.warning("CLIENTS PASSWORDS WILL BE DISPLAYED AS PLAIN TEXT IN DEBUG MODE")


class MailproxDB(object):
	"""
	Represents easy to use DB interface
	"""
	def __init__(self, db_file: str):
		# create db if it doesn't exist
		if not path.exists(db_file):
			log.info("Database file doesn't exists - will be created")

			sql_file = make_path_abs('crate_db.sql')

			with open(sql_file) as sql:
				self.db = sqlite3.connect(db_file)
				self.db.executescript(sql.read())
				self.db.close()

		self.db = sqlite3.connect(db_file)
		self.db.row_factory = sqlite3.Row  # Return rows as dictionary like object

		# admin user name (auth login without base64 encode for this user)
		self.admin_user_name = self._get_admin_user_name()

	def _get_admin_user_name(self) -> str:
		sql = "SELECT users.u_name FROM users WHERE u_id=0"
		cur = self.db.execute(sql)
		row = cur.fetchone()
		return row['u_name']

	def get_user_data(self, username: str) -> sqlite3.Row:
		"""
		:param username: Wanted username
		:return: User's row or None if user doesn't exist
		"""
		sql = "SELECT users.* FROM users WHERE u_name=?"
		cur = self.db.execute(sql, (username,))
		return cur.fetchone()

	def get_user_list(self) -> list:
		"""
		:return: List of all users
		"""
		sql = "SELECT users.u_name FROM users"
		cur = self.db.execute(sql)
		user_list = [row[0] for row in cur.fetchall()]
		return user_list

	def get_smtp(self) -> sqlite3.Row:
		"""
		:return: SMTP server configuration
		"""
		sql = "SELECT * FROM smtp WHERE s_id = 1"
		cur = self.db.execute(sql)
		row = cur.fetchone()
		return row

	def update_smtp(self, user: str, password: str, host: str, port: int, use_ssl: bool) -> None:
		"""
		Set new SMTP server configuration
		"""
		sql = "UPDATE smtp SET s_user=?, s_password=?, s_host=?, s_port=?, s_useSsl=? WHERE s_id=1"
		self.db.execute(sql, [user, password, host, port, use_ssl])
		self.db.commit()

	# noinspection PyPep8Naming,PyIncorrectDocstring
	def update_user(
			self,
			u_id: int,
			username: str, password_hash: str, comment: str, allow_no_SSL: bool, whitelist: str
	) -> None:
		"""
		Update user configuration
		:param u_id: Updated user id
		"""
		sql = "UPDATE users SET u_name=?, u_password=?, u_comment=?, u_allowNoSsl=?, u_whiteList=? WHERE u_id=?"
		self.db.execute(sql, [username, password_hash, comment, allow_no_SSL, whitelist, u_id])
		self.db.commit()
		self.admin_user_name = self._get_admin_user_name()

	# noinspection PyPep8Naming
	def add_user(self, username: str, password_hash: str, comment: str, allow_no_SSL: bool, whitelist: str) -> None:
		"""
		Add new user
		"""
		sql = "INSERT INTO users(u_name, u_password, u_comment, u_allowNoSsl, u_whiteList) VALUES (?, ? , ?, ?, ?)"
		self.db.execute(sql, [username, password_hash, comment, allow_no_SSL, whitelist])
		self.db.commit()
		self.admin_user_name = self._get_admin_user_name()

	def remove_user(self, u_id: int) -> None:
		sql = "DELETE FROM users WHERE u_id = ?"
		self.db.execute(sql, [u_id])
		self.db.commit()


""" Setup database """
db = MailproxDB(make_path_abs(config.db_file))


async def readline_timeout(reader: asyncio.StreamReader, timeout: float = 300) -> bytes:
	"""
	Raise asyncio.TimeoutError on timeout expired
	:param reader: reader object
	:param timeout: timeout
	:return: received data (bytes)
	"""
	read = await asyncio.wait_for(reader.readline(), timeout=timeout)
	return read


def prepare_cmd(recv: bytes, upper: bool = True) -> list:
	"""
	Decode bytes as SMTP command (list of str)
	:param recv: received bytes
	:param upper: if true all letter will be converted to large characters
	:return: list of strings
	"""
	cmd = recv[:-2]
	cmd = cmd.decode()
	while cmd.replace('  ', ' ') != cmd:
		cmd = cmd.replace('  ', ' ')
	if upper:
		cmd = cmd.upper()
	cmd = cmd.split(' ')
	return cmd


def auth_decode(auth: bytes) -> (str, None):
	"""
	Decode base64 decoded data
	:param auth: bytes to decoded
	:return: Decoded data(str) or None when impossible
	"""
	try:
		auth = base64.b64decode(auth, validate=True)
		auth = auth.decode()
	except (binascii.Error, UnicodeDecodeError):
		return None
	return auth


def decode_plain(plain: bytes) -> ((str, None), (str, None)):
	"""
	Decode base64 encoded SMTP format plain auth (\0username\0password)
	:param plain: data to decode (bytes)
	:return: tuple of string (username, password) or tuple (None, None) when impossible
	"""
	decoded = auth_decode(plain)

	if decoded is None:
		return None, None

	decoded = decoded.split('\0')

	if len(decoded) < 3:
		return None, None

	return decoded[1], decoded[2]


class MessageProcessor(ABC):
	"""
	Abstract base class for processing data received from clients
	"""
	def __init__(self, client_id: str) -> None:
		self.client_id: str = client_id

		self.stage: str = ""

	@abstractmethod
	def process(self, recv_from_client: bytes) -> bytes:
		"""
		Abstract
		:param recv_from_client: bytes received from client
		:return: bytes to send to client
		"""
		pass


class SMTPAuthServer(MessageProcessor):
	"""
	Used to authorized clients using SMTP protocol (AUTH PLAIN or AUTH LOGIN)
	"""

	STAGE_BEFORE_INIT = "STAGE_BEFORE_INIT"
	STAGE_BEFORE_HELO = "STAGE_BEFORE_HELO"
	STAGE_BEFORE_AUTH = "STAGE_BEFORE_AUTH"
	STAGE_BEFORE_AUTH_USERNAME = "STAGE_BEFORE_AUTH_USERNAME"
	STAGE_BEFORE_AUTH_PASSWORD = "STAGE_BEFORE_AUTH_PASSWORD"
	STAGE_BEFORE_AUTH_PLAIN = "STAGE_BEFORE_AUTH_PLAIN"
	STAGE_AUTHORIZED = "STAGE_AUTHORIZED"

	STAGE_CHECK_AUTH = "STAGE_CHECK_AUTH"
	STAGE_OPEN_PROXY = "STAGE_OPEN_PROXY"

	def __init__(self, client_id: str, is_ssl: bool, client_host: str) -> None:
		super().__init__(client_id)

		self.stage = self.STAGE_BEFORE_INIT

		self.is_ssl = is_ssl  # is connection encrypted
		self.client_host = client_host  # to validate whitelist

		# Client authority - password is removed from memory as soon as possible
		self.username = None
		self.password = None

		self.user_setup = None

	async def process(self, recv_from_client: bytes) -> bytes:
		msg_to_client = ""

		# QUIT command
		if self.stage in [self.STAGE_BEFORE_INIT, self.STAGE_BEFORE_HELO, self.STAGE_BEFORE_AUTH]:
			cmd = prepare_cmd(recv_from_client)
			if cmd[0] == "QUIT":
				# noinspection PyTypeChecker
				return None  # None meas close connection

		# Send server initial message 220 """
		if self.stage == self.STAGE_BEFORE_INIT:
			msg_to_client += "220 MAILPROX {}\r\n".format(datetime.now())
			self.stage = self.STAGE_BEFORE_HELO

		# Process HELO/EHLO from client and send 250 response
		elif self.stage == self.STAGE_BEFORE_HELO:
			helo_msg = "250-MAILPROX by Dominik Przybysz Hello\r\n"

			cmd = prepare_cmd(recv_from_client)

			if cmd[0] == "EHLO":
				msg_to_client += helo_msg
				msg_to_client += "250 AUTH LOGIN PLAIN\r\n"

				self.stage = self.STAGE_BEFORE_AUTH
			elif cmd[0] == "HELO":
				msg_to_client += helo_msg

				self.stage = self.STAGE_BEFORE_AUTH
			else:
				msg_to_client += "503 5.5.1 Error: send HELO/EHLO first\r\n"

		# Process AUTH command
		# Case 1: Send base64 "Username:" if AUTH LOGIN was initialized
		# Case 2: Decode username and password if AUTH PLAIN PLAIN_STRING was received
		# Case 3: Send 334 response if AUTH PLAIN was received
		elif self.stage == self.STAGE_BEFORE_AUTH:
			cmd = prepare_cmd(recv_from_client)

			if cmd[0] == 'AUTH':
				# Case 1
				if cmd[1] == "LOGIN":
					msg_to_client += "334 "
					msg_to_client += base64.b64encode("Username:".encode()).decode()
					msg_to_client += "\r\n"

					self.stage = self.STAGE_BEFORE_AUTH_USERNAME

				elif cmd[1] == "PLAIN":
					# Case 2
					if len(cmd) > 2:
						self.username, self.password = decode_plain(prepare_cmd(recv_from_client, upper=False)[2])
						self.stage = self.STAGE_CHECK_AUTH
					# Case 3
					else:
						msg_to_client += "334\r\n"
						self.stage = self.STAGE_BEFORE_AUTH_PLAIN
				else:
					msg_to_client = "504 5.7.4 Unrecognized authentication type\r\n"
			else:
				msg_to_client = "530 Authentication required\r\n"

		# Decode received username and send base64 encoded "Password:" to client
		elif self.stage == self.STAGE_BEFORE_AUTH_USERNAME:
			if recv_from_client[:-2].decode() == db.admin_user_name:
				self.username = db.admin_user_name
			else:
				self.username = auth_decode(recv_from_client[:-2])

			msg_to_client += "334 "
			msg_to_client += base64.b64encode("Password:".encode()).decode()
			msg_to_client += "\r\n"
			self.stage = self.STAGE_BEFORE_AUTH_PASSWORD

		# Decode received password and switch to STAGE_CHECK_AUTH
		elif self.stage == self.STAGE_BEFORE_AUTH_PASSWORD:
			if self.username == db.admin_user_name:
				self.password = recv_from_client[:-2].decode()
			else:
				self.password = auth_decode(recv_from_client[:-2])
			self.stage = self.STAGE_CHECK_AUTH

		# Decode PLAIN message and switch to STAGE_CHECK_AUTH
		elif self.stage == self.STAGE_BEFORE_AUTH_PLAIN:
			self.username, self.password = decode_plain(recv_from_client[:-2])
			self.stage = self.STAGE_CHECK_AUTH

		# Check auth and send 235 or 535 response
		if self.stage == self.STAGE_CHECK_AUTH:
			authorized = False

			if self.username is not None and self.password is not None:
				user_data = db.get_user_data(self.username)

				if user_data is not None:
					whitelist_re = r"^(.*,)?\s*"+re.escape(self.client_host)+r"\s*(,?.*)?$"

					# Check SSL condition
					if not self.is_ssl and not user_data['u_allowNoSsl']:
						log.warning("Login of {} from {} denied due to SSL condition".format(self.username, self.client_host))
					# Check white list condition
					elif user_data['u_whiteList'] != "" and re.match(whitelist_re, user_data['u_whiteList']) is None:
						log.warning("Login of {} from {} denied due to whitelist condition".format(self.username, self.client_host))
					else:
						authorized = hash.verify_password(user_data['u_password'], self.password)

			if authorized:
				msg_to_client = "235 OK Authenticated\r\n"
				self.password = None
				self.stage = self.STAGE_AUTHORIZED

				log.info("User {} from {} authenticated successfully".format(self.username, self.client_id))
			else:
				msg_to_client = "535 5.7.1 Authentication failed\r\n"
				self.username = None
				self.stage = self.STAGE_BEFORE_AUTH
				log.warning("User {} from {} authentication failed".format(self.username, self.client_id))
			self.password = None

		return msg_to_client.encode()


class AdminProcessor(MessageProcessor):
	STAGE_ADMIN = "STAGE_ADMIN"

	STAGE_SMTP_SERVER_CONFIG = "STAGE_SMTP_SERVER_CONFIG"
	STAGE_SMTP_SERVER_USER = "STAGE_SMTP_SERVER_USER"
	STAGE_SMTP_SERVER_PASSWORD = "STAGE_SMTP_SERVER_PASSWORD"
	STAGE_SMTP_SERVER_HOST = "STAGE_SMTP_SERVER_HOST"
	STAGE_SMTP_SERVER_PORT = "STAGE_SMTP_SERVER_PORT"
	STAGE_SMTP_SERVER_SSL = "STAGE_SMTP_SERVER_SSL"
	STAGE_SMTP_SERVER_COMMIT = "STAGE_SMTP_SERVER_COMMIT"

	STAGE_USER_ADDMOD_NAME = "STAGE_USER_ADDMOD_NAME"
	STAGE_USER_ADDMOD_PASSWORD = "STAGE_USER_ADDMOD_PASSWORD"
	STAGE_USER_ADDMOD_WHITELIST = "STAGE_USER_ADDMOD_HOST"
	STAGE_USER_ADDMOD_ALLOW_NO_SSL = "STAGE_USER_ADDMOD_USE_SSL"
	STAGE_USER_ADDMOD_COMMENT = "STAGE_USER_ADDMOD_COMMENT"
	STAGE_USER_ADDMOD_COMMIT = "STAGE_USER_ADDMOD_COMMIT"

	STAGE_USER_DEL_COMMIT = "STAGE_USER_DEL_COMMIT"

	STAGE_USER_DEL = "STAGE_USER_DEL"

	class MenuFiled(object):
		"""
		Class represents one input filed and implements validation.
		"""
		def __init__(
				self,
				label: str, db_key: str,
				next_stage: (str, None),
				is_bool: bool = False, is_hashed: bool = False, is_username: bool = False,
				allow_empty: bool = False
		):
			self.label = label  # Displayed label
			self.db_key = db_key  # Key in database
			self.next_stage = next_stage  # Next stage after this (str or None)
			self.is_bool = is_bool  # is bool (yes/no) value
			self.is_hashed = is_hashed  # is hashed password
			self.allow_empty = allow_empty
			self.is_username = is_username  # is username - must be unique

		def get_default(self, orginal_dict: dict) -> str:
			"""
			:param orginal_dict: source of default value
			:return: Default value from on orginal dict and own database key
			"""
			return orginal_dict[self.db_key]

		def create_input(self, orginal_dict: dict) -> str:
			"""
			:param orginal_dict: dict which contains default value
			:return: Message prompt for value
			"""

			default = self.get_default(orginal_dict)

			if self.is_hashed and default != "":
				default = "OLD"

			if self.is_bool:
				default = "YES/no" if default else "yes/NO"

			return "{}[{}]:".format(self.label, default)

		def process_input(
				self,
				recv_from_client: str,
				orginal_dict: dict, destination_dict: dict
		) -> (str, None):
			"""
			Processes received from client reaction for prompt
			:param recv_from_client: received string
			:param orginal_dict: dictionary with default value
			:param destination_dict: dictionary where received value will be saved
			:return: None or error message to client with repeated prompt
			"""
			recv_from_client = recv_from_client.strip()

			default = orginal_dict[self.db_key]

			input_msg = self.create_input(orginal_dict)

			if self.is_bool:
				if recv_from_client.upper() == "YES":
					destination_dict[self.db_key] = True
					return None
				elif recv_from_client.upper() == "NO":
					destination_dict[self.db_key] = False
					return None
				elif recv_from_client == "":
					destination_dict[self.db_key] = default
					return None
				else:
					return "Incorrect value. " + input_msg

			if not self.allow_empty and default == "" and recv_from_client == "":
				return "Empty value not allowed. " + input_msg

			# check whitespace in username
			if self.is_username and re.match(r".*\s.*", recv_from_client) is not None:
				return "User name can not contain any whitespace. " + input_msg

			# check is username unique
			if self.is_username and recv_from_client != default:
				if recv_from_client in db.get_user_list():
					return "User like this already exists. " + input_msg

			if recv_from_client == "":
				destination_dict[self.db_key] = default
				return None

			if self.is_hashed:
				destination_dict[self.db_key] = hash.hash_password(recv_from_client)
				return None

			destination_dict[self.db_key] = recv_from_client
			return None

	def __init__(self, client_id: str) -> None:
		super().__init__(client_id)

		# Dictionary of fields for each stage
		self.menu_items = {
			self.STAGE_SMTP_SERVER_USER: self.MenuFiled('Username', 's_user', self.STAGE_SMTP_SERVER_PASSWORD),
			self.STAGE_SMTP_SERVER_PASSWORD: self.MenuFiled('Password', 's_password', self.STAGE_SMTP_SERVER_HOST),
			self.STAGE_SMTP_SERVER_HOST: self.MenuFiled('Host', 's_host', self.STAGE_SMTP_SERVER_PORT),
			self.STAGE_SMTP_SERVER_PORT: self.MenuFiled('Port', 's_port', self.STAGE_SMTP_SERVER_SSL),
			self.STAGE_SMTP_SERVER_SSL: self.MenuFiled('Use SSL', 's_useSsl', self.STAGE_SMTP_SERVER_COMMIT, is_bool=True),
			self.STAGE_SMTP_SERVER_COMMIT: self.MenuFiled('Commit', 'commit', None, is_bool=True),

			self.STAGE_USER_ADDMOD_NAME: self.MenuFiled('Username', 'u_name', self.STAGE_USER_ADDMOD_PASSWORD, is_username=True),
			self.STAGE_USER_ADDMOD_PASSWORD: self.MenuFiled(
				'Password', 'u_password', self.STAGE_USER_ADDMOD_ALLOW_NO_SSL, is_hashed=True
			),
			self.STAGE_USER_ADDMOD_ALLOW_NO_SSL: self.MenuFiled(
				'Allow No SSL', 'u_allowNoSsl', self.STAGE_USER_ADDMOD_WHITELIST, is_bool=True
			),
			self.STAGE_USER_ADDMOD_WHITELIST: self.MenuFiled(
				'Hosts whitelist(comma separated)', 'u_whiteList', self.STAGE_USER_ADDMOD_COMMENT, allow_empty=True
			),
			self.STAGE_USER_ADDMOD_COMMENT: self.MenuFiled(
				'Comment', 'u_comment', self.STAGE_USER_ADDMOD_COMMIT, allow_empty=True
			),
			self.STAGE_USER_ADDMOD_COMMIT: self.MenuFiled('Commit', 'commit', None, is_bool=True),
			self.STAGE_USER_DEL_COMMIT: self.MenuFiled('Commit', 'commit', None, is_bool=True),
		}

		self.orginal_dict = None
		self.new_dict = None

	# noinspection PyMethodMayBeStatic
	def help(self) -> str:
		"""
		:return: Help message
		"""
		msg = "--------\r\n"
		msg += "ADMIN MODE:\r\n"
		msg += "HELP\r\n"
		msg += "SMTP SERVER CONFIG\r\n"
		msg += "SMTP SERVER TEST\r\n"
		msg += "USER LIST\r\n"
		msg += "USER ADD\r\n"
		msg += "USER MOD [USER_NAME]\r\n"
		msg += "USER DEL [USER_NAME]\r\n"
		msg += "LOG LEVEL [critical|error|warning|INFO|debug]\r\n"
		msg += "QUIT\r\n"
		msg += "--------\r\n"
		return msg

	async def open(self) -> bytes:
		"""
		Initial method for processor
		:return: Help message
		"""
		self.stage = self.STAGE_ADMIN

		return self.help().encode()

	async def process(self, recv_from_client: bytes) -> (bytes, None):
		"""
		Process commands from client
		:param recv_from_client:  received data from client
		:return: encoded response to client or None if connection to client should be closed
		"""
		msg_to_client = ""

		# Main menu stage
		if self.stage == self.STAGE_ADMIN:
			cmd = prepare_cmd(recv_from_client)

			# QUIT command
			if cmd[0] == "QUIT":
				log.info("Connection to {} will be closed after QUIT".format(self.client_id))
				# noinspection PyTypeChecker
				return None  # None means close connection to client

			# HELP command
			elif cmd[0] == "HELP":
				msg_to_client += self.help()

			# enter SMTP server configuration
			elif cmd[0] == "SMTP" and cmd[1] == "SERVER" and cmd[2] == "CONFIG":
				self.orginal_dict = db.get_smtp()
				self.orginal_dict = dict(self.orginal_dict)
				self.orginal_dict['commit'] = False
				self.new_dict = {}

				self.stage = self.STAGE_SMTP_SERVER_USER
				msg_to_client += "--------\r\n"
				msg_to_client += self.menu_items[self.stage].create_input(self.orginal_dict)

			# enter USER ADD menu
			elif cmd[0] == "USER" and cmd[1] == "ADD":
				# noinspection PyDictCreation
				self.orginal_dict = {
					'u_name': '', 'u_password': '', 'u_comment': '', 'u_smtp': '', 'u_allowNoSsl': '', 'u_whiteList': ''
				}
				self.orginal_dict['commit'] = False

				self.new_dict = {}

				self.stage = self.STAGE_USER_ADDMOD_NAME
				msg_to_client += "--------\r\n"
				msg_to_client += self.menu_items[self.stage].create_input(self.orginal_dict)

			# enter USER MOD menu
			elif cmd[0] == "USER" and cmd[1] == "MOD":
				if len(cmd) > 2:
					self.orginal_dict = db.get_user_data(prepare_cmd(recv_from_client, upper=False)[2])
					if self.orginal_dict is not None:
						self.orginal_dict = dict(self.orginal_dict)
						self.orginal_dict['commit'] = False

						self.new_dict = {}

						self.stage = self.STAGE_USER_ADDMOD_NAME
						msg_to_client += "--------\r\n"
						msg_to_client += self.menu_items[self.stage].create_input(self.orginal_dict)
					else:
						msg_to_client += "Unknown user\r\n"
				else:
					msg_to_client += "Specify username\r\n"

			# prepare to remove user
			elif cmd[0] == "USER" and cmd[1] == "DEL":
				if len(cmd) > 2:
					self.orginal_dict = db.get_user_data(prepare_cmd(recv_from_client, upper=False)[2])
					if self.orginal_dict is not None:
						if self.orginal_dict['u_id'] == 0:
							msg_to_client += "Admin user can not be removed"
						else:
							self.orginal_dict = dict(self.orginal_dict)
							self.orginal_dict['commit'] = False

							self.new_dict = {}

							self.stage = self.STAGE_USER_DEL_COMMIT
							msg_to_client += self.menu_items[self.stage].create_input(self.orginal_dict)
					else:
						msg_to_client += "Unknown user\r\n"
				else:
					msg_to_client += "Specify username\r\n"

			# send list of users
			elif cmd[0] == "USER" and cmd[1] == "LIST":
				msg_to_client += "--------\r\n"
				msg_to_client += "\r\n".join(db.get_user_list())
				msg_to_client += "\r\n"
				msg_to_client += "--------\r\n"

			# test SMTP server
			elif cmd[0] == "SMTP" and cmd[1] == "SERVER" and cmd[2] == "TEST":
				proxy = SMTPProxy("SMTP SERVER TEST")
				try:
					await proxy.open()
					proxy.smtp_server_response_wanted = False
					await proxy.process("QUIT".encode())
				except SMTPProxy.SMTPProxyError as ex:
					msg_to_client += "FAILED: {}".format(ex)
				except Exception as ex:
					msg_to_client += "FAILED - UNKNOWN ERROR: {}".format(ex)
				else:
					msg_to_client += "PASSED"
				msg_to_client += "\r\n--------\r\n"

			# set log level
			elif cmd[0] == "LOG" and cmd[1] == "LEVEL":
				if len(cmd) == 2:
					cmd.append("INFO")

				try:
					log.setLevel(cmd[2])
					log.info("Log level changed to {}".format(cmd[2]))
					msg_to_client += "Log level changed to {}\r\n--------\r\n".format(cmd[2])
				except ValueError:
					msg_to_client += "Incorrect log level\r\n"

			# unknown command
			else:
				msg_to_client += "Unknown command\r\n"
				msg_to_client += self.help()

		# process complicated features substages
		elif self.stage in [
			self.STAGE_SMTP_SERVER_USER, self.STAGE_SMTP_SERVER_PASSWORD, self.STAGE_SMTP_SERVER_HOST,
			self.STAGE_SMTP_SERVER_PORT, self.STAGE_SMTP_SERVER_SSL, self.STAGE_SMTP_SERVER_COMMIT,
			self.STAGE_USER_ADDMOD_NAME, self.STAGE_USER_ADDMOD_PASSWORD, self.STAGE_USER_ADDMOD_WHITELIST,
			self.STAGE_USER_ADDMOD_ALLOW_NO_SSL, self.STAGE_USER_ADDMOD_COMMENT, self.STAGE_USER_ADDMOD_COMMIT,
			self.STAGE_USER_DEL_COMMIT
		]:
			# decode message from client
			recv_from_client = recv_from_client[:-2].decode()

			# process received data as response for prompt
			input_process_result = self.menu_items[self.stage].process_input(
				recv_from_client, self.orginal_dict, self.new_dict
			)

			# Send process error message if occurred
			if input_process_result is not None:
				msg_to_client += input_process_result

			# Save updated SMTP server configuration or discard
			elif self.stage == self.STAGE_SMTP_SERVER_COMMIT:
				if self.new_dict['commit']:
					db.update_smtp(
						self.new_dict['s_user'], self.new_dict['s_password'],
						self.new_dict['s_host'], self.new_dict['s_port'],
						self.new_dict['s_useSsl']
					)
					msg_to_client += "SAVED\r\n--------\r\n"
				else:
					msg_to_client += "DISCARDED\r\n--------\r\n"

				self.stage = self.STAGE_ADMIN

			# Save new or updated user
			elif self.stage == self.STAGE_USER_ADDMOD_COMMIT:
				if self.new_dict['commit']:

					# Add new user
					if self.orginal_dict['u_name'] == "":
						db.add_user(
							self.new_dict['u_name'], self.new_dict['u_password'],
							self.new_dict['u_comment'],
							self.new_dict['u_allowNoSsl'], self.new_dict['u_whiteList']
						)
						msg_to_client += "SAVED\r\n--------\r\n"

					# Update old user
					else:
						db.update_user(
							self.orginal_dict['u_id'],
							self.new_dict['u_name'], self.new_dict['u_password'],
							self.new_dict['u_comment'],
							self.new_dict['u_allowNoSsl'], self.new_dict['u_whiteList']
						)
						msg_to_client += "SAVED\r\n--------\r\n"

				else:
					msg_to_client += "DISCARDED\r\n--------\r\n"

				self.stage = self.STAGE_ADMIN

			# Remove user
			elif self.stage == self.STAGE_USER_DEL_COMMIT:
				if self.new_dict['commit']:
					db.remove_user(self.orginal_dict['u_id']					)
					msg_to_client += "REMOVED\r\n--------\r\n"
				else:
					msg_to_client += "DISCARDED\r\n--------\r\n"

				self.stage = self.STAGE_ADMIN

			# Go to next substage
			else:
				self.stage = self.menu_items[self.stage].next_stage
				msg_to_client += self.menu_items[self.stage].create_input(self.orginal_dict)

		# noinspection PyTypeChecker
		return msg_to_client.encode()


class SMTPProxy(MessageProcessor):
	"""
	Open connection to SMTP server and forward messages between client and SMTP server
	"""

	STAGE_BEFORE_DATA = "STAGE_BEFORE_DATA"
	STAGE_DATA = "STAGE_DATA"
	STAGE_AFTER_DATA = "STAGE_AFTER_DATA"

	class SMTPProxyError(Exception):
		pass

	def __init__(self, client_id: str):
		super().__init__(client_id)

		# True if response from SMTP server is expected
		self.smtp_server_response_wanted = True

		self.smtp_server_config = db.get_smtp()

		self.smtp_server_host = self.smtp_server_config['s_host']
		self.smtp_server_port = self.smtp_server_config['s_port']
		self.smtp_server_username = self.smtp_server_config['s_user']
		self.smtp_server_password = self.smtp_server_config['s_password']
		self.smtp_server_use_ssl = bool(self.smtp_server_config['s_useSsl'])

		self.identifier = "MAILPROX"

		self.smtp_server_writer = None
		self.smtp_server_reader = None

	async def open(self) -> bytes:
		"""
		Open connection to SMTP server and login
		"""
		log.debug(
			"Opening connection to {}:{} for {}".format(self.smtp_server_host, self.smtp_server_port, self.client_id)
		)
		self.smtp_server_reader, self.smtp_server_writer = \
			await asyncio.open_connection(self.smtp_server_host, self.smtp_server_port, ssl=self.smtp_server_use_ssl)

		# Receive hello initial message from SMTP server
		recv_from_smtp_server = await readline_timeout(self.smtp_server_reader)
		log.debug(
			"Received {} from {}:{} for {}".format(
				recv_from_smtp_server, self.smtp_server_host, self.smtp_server_port, self.client_id
			)
		)
		cmd = prepare_cmd(recv_from_smtp_server)
		if cmd[0] != "220":
			raise self.SMTPProxyError("Can not set up communication with SMTP server")

		# Send EHLO to SMTP server
		msg_to_smtp_server = "EHLO {}\r\n".format(self.identifier)
		log.debug(
			"Send {} to {}:{} for {}".format(
				msg_to_smtp_server, self.smtp_server_host, self.smtp_server_port, self.client_id
			)
		)
		self.smtp_server_writer.write(msg_to_smtp_server.encode())
		await self.smtp_server_writer.drain()

		# Receive 250 response from SMTP server and check AUTH PLAIN support
		plain_support = False
		re_plain = re.compile(r"250[ -]AUTH[ =](\w+ )*PLAIN( \w+)*")
		while True:
			recv_from_smtp_server = await readline_timeout(self.smtp_server_reader)
			log.debug(
				"Received {} from {}:{} for {}".format(
					recv_from_smtp_server, self.smtp_server_host, self.smtp_server_port, self.client_id
				)
			)
			read = recv_from_smtp_server[:-2].decode()
			if read[:4] in ["250-", "250", "250 "]:
				if not plain_support:
					plain_support = (re.match(re_plain, read) is not None)

				if read[:4] in ["250", "250 "]:
					break
				continue
			raise self.SMTPProxyError("SMTP protocol error - 250 code expected")

		if not plain_support:
			raise self.SMTPProxyError("SMTP error - AUTH PLAIN not supported")

		plain = "\0"+self.smtp_server_username+"\0"+self.smtp_server_password
		plain = plain.encode()
		plain = base64.b64encode(plain)
		plain = plain.decode()

		msg_to_smtp_server = "AUTH PLAIN {}\r\n".format(plain)
		msg_to_smtp_server = msg_to_smtp_server.encode()
		log.debug(
			"Send {} to {}:{} for {}".format(
				msg_to_smtp_server, self.smtp_server_host, self.smtp_server_port, self.client_id
			)
		)
		self.smtp_server_writer.write(msg_to_smtp_server)
		await self.smtp_server_writer.drain()

		# Process authorization response
		recv_from_smtp_server = await readline_timeout(self.smtp_server_reader)
		log.debug(
			"Received {} from {}:{} for {}".format(
				recv_from_smtp_server, self.smtp_server_host, self.smtp_server_port, self.client_id
			)
		)
		cmd = prepare_cmd(recv_from_smtp_server)

		if cmd[0] == "535":
			raise self.SMTPProxyError("SMTP error - Authentication failed")

		if cmd[0] != "235":
			raise self.SMTPProxyError("SMTP error - Authentication error")

		log.info(
			"Opened connection to {}:{} for {}".format(self.smtp_server_host, self.smtp_server_port, self.client_id)
		)

		return b""

	async def process(self, recv_from_client: bytes) -> bytes:
		"""
		Forward messages between clients and SMTP server
		"""

		# Send data received from client to SMTP server
		self.smtp_server_writer.write(recv_from_client)
		await self.smtp_server_writer.drain()

		# Catch QUIT command
		if \
			self.smtp_server_writer.is_closing() or \
			(self.smtp_server_response_wanted and prepare_cmd(recv_from_client)[0] == "QUIT"):

			log.info("Connection for {} will be closed after SMTP server connection close".format(self.client_id))
			# noinspection PyTypeChecker
			return None  # None means close connection

		# if mail content is transmitted look for finish symbol ".\r\n"
		if not self.smtp_server_response_wanted and recv_from_client.decode() == ".\r\n":
			self.smtp_server_response_wanted = True

		# Receive response from SMTP server
		if self.smtp_server_response_wanted:
			recv_from_smtp_server = await self.smtp_server_reader.readline()

			# Detect mail content transmission begin
			client_cmd = prepare_cmd(recv_from_client)
			smtp_server_cmd = prepare_cmd(recv_from_smtp_server)
			if client_cmd[0] == "DATA" and smtp_server_cmd[0] == "354":
				self.smtp_server_response_wanted = False
		# If data from SMTP server are not expected check is buffer empty and return no data to send to client
		else:
			# noinspection PyProtectedMember
			if len(self.smtp_server_reader._buffer) > 0:
				log.warning("There ara unexpected data in buffer {} from {}:{}".format(None, None, None))
			recv_from_smtp_server = "".encode()

		return recv_from_smtp_server


class TcpClient(object):
	"""
	Each TCP client object
	"""
	def __init__(self, reader, writer):
		self.reader = reader
		self.writer = writer

		self.processor = None

	async def engage(self):
		own_ip, own_port = self.writer.get_extra_info('sockname')
		client_ip, client_port = self.writer.get_extra_info('peername')
		client_id = "{}:{}".format(client_ip, client_port)

		log.info("Connection from {} on {}:{}".format(client_id, own_ip, own_port))

		self.processor = SMTPAuthServer(client_id, own_port == config.ssl_port, client_ip)

		try:
			recv_from_client = "".encode()
			while True:
				msg_for_client = await self.processor.process(recv_from_client)

				if msg_for_client is None:
					break

				# Open connection to SMTP server after authorization
				if isinstance(self.processor, SMTPAuthServer) and self.processor.stage == self.processor.STAGE_AUTHORIZED:
					if self.processor.username == db.admin_user_name:
						self.processor = AdminProcessor(client_id)
						msg_for_client += await self.processor.open()
					else:
						self.processor = SMTPProxy(client_id)
						msg_for_client += await self.processor.open()

				# Send data to client if there are any
				if msg_for_client is not None and len(msg_for_client) > 0:
					self.writer.write(msg_for_client)
					await self.writer.drain()
					log.debug("Send {} to {}".format(msg_for_client, client_ip))

				# Receive data from client
				recv_from_client = await asyncio.wait_for(self.reader.readline(), 300)
				log.debug("Received {} from {}".format(recv_from_client, client_id))

				# Reaction to connection closed by client
				if recv_from_client is not None and len(recv_from_client) == 0:
					log.debug("Connection to {} was closed by client".format(client_id))
					break
		except asyncio.exceptions.TimeoutError:
			log.error("Timeout expired during communication with {}".format(client_id))
		except Exception as ex:
			log.exception(ex)

		self.writer.close()


# Coroutine to handle clients
async def handle_client(reader, writer):
	client = TcpClient(reader, writer)
	await client.engage()

STOP_EVENT = asyncio.Event()


async def server_coroutine():
	no_ssl_server = None
	ssl_server = None

	if config.no_ssl_address is not None:
		no_ssl_server = await asyncio.start_server(handle_client, config.no_ssl_address, config.no_ssl_port)
		ip, port = no_ssl_server.sockets[0].getsockname()
		log.info("Started no SSL server on socket {}:{}".format(ip, port))

	if config.ssl_address is not None:
		config.ssl_cert_file = make_path_abs(config.ssl_cert_file)
		config.ssl_key_file = make_path_abs(config.ssl_key_file)

		context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
		context.load_cert_chain(config.ssl_cert_file, config.ssl_key_file)

		ssl_server = await asyncio.start_server(
			handle_client,
			config.ssl_address, config.ssl_port,
			ssl=context
		)

		ip, port = ssl_server.sockets[0].getsockname()
		log.info("Started SSL server on socket {}:{}".format(ip, port))

	await STOP_EVENT.wait()

	if no_ssl_server is not None:
		no_ssl_server.close()
	if ssl_server is not None:
		ssl_server.close()


def main() -> None:
	loop = asyncio.get_event_loop()

	main_task = asyncio.ensure_future(server_coroutine())

	for signal_name in [signal.SIGINT, signal.SIGTERM]:
		loop.add_signal_handler(signal_name, main_task.cancel)

	try:
		loop.run_until_complete(main_task)
	except asyncio.exceptions.CancelledError:
		pass
	finally:
		STOP_EVENT.set()
		sleep_start = time.time()
		while len(asyncio.all_tasks(loop)) > 0:
			if time.time() - sleep_start > 5:
				log.error("Can not stop program gracefully")
				break
			time.sleep(0.1)
		loop.close()


main()
