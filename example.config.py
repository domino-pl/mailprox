import logging

db_file = "mailprox.db"
log_file = "mailprox.log"  # additional log file - None to disable

# No SSL and SSL connection can not share same port on different interfaces !!!

no_ssl_address = "127.0.0.1"
no_ssl_port = 2525  # None to disable

ssl_address = "0.0.0.0"
ssl_port = 25465  # None to disable

ssl_cert_file = "cert.pem"
ssl_key_file = "key.pem"

log_level = logging.INFO  # CRITICAL|ERROR|WARNING|INFO|DEBUG
