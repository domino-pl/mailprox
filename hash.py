# Based on: https://www.vitoshacademy.com/hashing-passwords-in-python/

import hashlib
import binascii
import os
import argparse


def hash_password(password):
    """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwd_hash = hashlib.pbkdf2_hmac(
        'sha512', password.encode('utf-8'),
        salt,
        100000
    )
    pwd_hash = binascii.hexlify(pwd_hash)
    return (salt + pwd_hash).decode('ascii')


def verify_password(stored_password, provided_password):
    """Verify a stored password against one provided by user"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwd_hash = hashlib.pbkdf2_hmac(
        'sha512', provided_password.encode('utf-8'),
        salt.encode('ascii'),
        100000
    )
    pwd_hash = binascii.hexlify(pwd_hash).decode('ascii')
    return pwd_hash == stored_password


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Return hashed password')
    parser.add_argument('password', help='Password to hash or to compare with provided hash')
    parser.add_argument('-c', '--check', help='Hash to verify', required=False)

    args = parser.parse_args()

    if args.check is None:
        print(hash_password(args.password))
    else:
        print(verify_password(args.check, args.password))
