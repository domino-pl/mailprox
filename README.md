# mailProx

---
## Main features

- Proxy beetwen local clients and remote SMTP server
- Auth local users using `AUTH LOGIN` or `AUTH PLAIN` methods
- Store local users and SMTP server configuration in SQLite database
- Manage this configuration via telnet management interface

---
## Configuration
### Config file
Copy `example.config.py` to `config.py` and specify options like used ports
### Connect to admin interface
Start `mailprox.py` and connect via telnet to admin interface (localhost only):
 - login as admin user (`ehlo`;`auth login`;username: `mailproxadmin`, password: `proxy`)
 - change admin password (`user mod mailproxadmin`)
 - setup SMTP server (`smtp server config`)
 - test SMTP server (`smtp server test`)
 - add user (`user add`)
 - exit (`quit`)

![Configuration screen](img/config_screen.png)

---
## SSL configuration
### Conection to encrypted server
`openssl s_client -crlf -connect localhost:25465`
### Certificate generation
`openssl req  -nodes -new -x509  -keyout key.pem -out cert.pem`