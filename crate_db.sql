BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "smtp" (
	"s_id"	INTEGER,
	"s_user"	TEXT,
	"s_password"	TEXT,
	"s_host"	TEXT,
	"s_port"	INTEGER,
	"s_useSsl"	INTEGER
);
CREATE TABLE IF NOT EXISTS "users" (
	"u_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"u_name"	TEXT,
	"u_password"	TEXT,
	"u_comment"	TEXT,
	"u_allowNoSsl"	INTEGER,
	"u_whiteList"	TEXT
);
INSERT INTO "smtp" ("s_id","s_user","s_password","s_host","s_port","s_useSsl") VALUES (1,'','','',465,1);
INSERT INTO "users" ("u_id","u_name","u_password","u_comment","u_allowNoSsl","u_whiteList") VALUES (0,'mailproxadmin','d097d72b272138a69e8c8933c2d011e72d9810a41ba586bd4487eecac722c228d4405e75154194cf4356ff78720d672df6c02eafff2fb5d66b5d1a0d5822e73fe0145f1861f6acc41bdd0c90c3915562c0bd64697e6bc0894e186d112d5d7dbf','Admin user',1,'127.0.0.1');
COMMIT;
